package io.nexo.balanace.calculator;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Find all the transaction of the SearchedUser and calculate the balance in USD based on the provided rates.
 */
public class Startup {

    public static void main(String[] args) {

        Map<String, BigDecimal> usdRates =
                Map.of("ETH", BigDecimal.valueOf(3500),
                        "BTC", BigDecimal.valueOf(46000));

        UUID searchedUser = UUID.randomUUID();
        List<Transaction> transactions = List.of(
                new Transaction(searchedUser, "ETH", BigDecimal.valueOf(0.3)),
                new Transaction(UUID.randomUUID(), "ETH", BigDecimal.valueOf(0.3)),
                new Transaction(UUID.randomUUID(), "ETH", BigDecimal.valueOf(0.3)),
                new Transaction(UUID.randomUUID(), "BTC", BigDecimal.valueOf(0.3)),
                new Transaction(UUID.randomUUID(), "BTC", BigDecimal.valueOf(0.6)),
                new Transaction(searchedUser, "BTC", BigDecimal.valueOf(0.45)),
                new Transaction(searchedUser, "ETH", BigDecimal.valueOf(0.44)),
                new Transaction(searchedUser, "BTC", BigDecimal.valueOf(0.335))
        );

    }
}
