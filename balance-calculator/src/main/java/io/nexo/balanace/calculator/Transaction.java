package io.nexo.balanace.calculator;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

public class Transaction {

    private final UUID userId;
    private final String currencyCode;
    private final BigDecimal amount;

    public Transaction(UUID userId, String currencyThicker, BigDecimal amount) {
        this.userId = userId;
        this.currencyCode = currencyThicker;
        this.amount = amount;
    }

    public UUID getUserId() {
        return userId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transaction that)) return false;
        return Objects.equals(userId, that.userId) && Objects.equals(currencyCode, that.currencyCode) && Objects.equals(amount, that.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, currencyCode, amount);
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "userId=" + userId +
                ", currencyCode='" + currencyCode + '\'' +
                ", amount=" + amount +
                '}';
    }
}